# Hummingbird for Linux and macOS

#### AirVPN's free and open source OpenVPN 3 client based on AirVPN's OpenVPN 3 library fork

### Version 1.1.1 - Release date 7 January 2021


## Note on Checksum Files

Hummingbird is an open source project and, as such, its source code can be
downloaded, forked and modified by anyone who wants to create a derivative
project or build it on his or her computer. This also means the source code can
be tampered or modified in a malicious way, therefore creating a binary version
of hummingbird which may act harmfully, destroy or steal your data, redirecting
your network traffic and data while pretending to be the "real" hummingbird
client genuinely developed and supported by AirVPN.

For this reason, we cannot guarantee forked, modified and custom compiled
versions of Hummingbird to be compliant to our specifications, development and
coding guidelines and style, including our security standards. These projects,
of course, may also be better and more efficient than our release, however we
cannot guarantee or provide help for the job of others.

You are therefore strongly advised to check and verify the checksum codes found
in the `.sha512` files to exactly correspond to the ones below, that is, the
checksum we have computed from the sources and distribution files directly
compiled and built by AirVPN. This will make you sure about the origin and
authenticity of the hummingbird client. Please note the files contained in the
distribution tarballs are created from the very source code available in the
master branch of the [official hummingbird's repository](https://gitlab.com/AirVPN/hummingbird).


### Checksum codes for Version 1.1.1

The checksum codes contained in files `hummingbird-<os>-<arch>-1.1.0.<archive>.sha512`
and `hummingbird.sha512` must correspond to the codes below in order to prove
they are genuinely created and distributed by AirVPN.

## Linux i686

`hummingbird-linux-i686-1.1.1.tar.gz`:
3afd37013713bc8900b5071512c55a899f769763813f82390061ef7591fe1e4f8660d950f36d352fd2aecf71cf55549855d710f81b01ee9b2d326cd325066d3d

`hummingbird`:
a9141413e53a62c43d8470e9fdb946ac107271e13ebfe962636e144fa3cfd3bcd9815c3458812ee81b1c1e3a9f572e9e2b5a027641e79f507e6181c413a3e8b5


## Linux x86_64

`hummingbird-linux-x86_64-1.1.1.tar.gz`:
c1bc5813c057a4cd7ebd71834ad3905abfabb99a53e4fae52dfdac70edad13731227ea37a1dc2e3fe87b81831d249193ff9d9d005a038580eb557949754b0e51

`hummingbird`:
4c325fcda4d7b5a003bf424a79f5219608520438f76061eb54e6038a9d62231af7c0d0d4606926f6855eaecfee4cf4de402602fe4980c9eefe6591afdfbb7329


## Linux ARM32

`hummingbird-linux-armv7l-1.1.1.tar.gz`:
caee78fcfe82027d712a04d6d1969a11c89f1fd408020e35ca97051cc7b6233e946053d5d9784096225746ac1eb84d4096a12f9bfc5f11c423c3b9191cfb3640

`hummingbird`:
ea40ab29c554cec2e6ae12da31abe1caf9b87f34928e448fb6cf784c187f20cdeca00f60f4cc622aa8fdca47a56807c64dcc14ea1b721c9763d5e1cb858d3f61


## Linux ARM64

`hummingbird-linux-aarch64-1.1.1.tar.gz`:
ef45a3a70e82d249c8a6944ad525cf67140c402a57a18f3dc754ea657a7c203a00030ef2ee688cbde6d9df9e524827d94b23cc4d02cec751548ede7dd428eb5a

`hummingbird`:
1d8da4afe797dc19ae3716bd2f288dd443dd267b50bd26f0dd93b1177b9bcbaf262dc9f67c920ff7d17af50223919ff0dce9a226791a0ff0f7acf66e54bd26eb


## macOS plain

`hummingbird-macos-1.1.1.tar.gz`:
cebc531b70ab5e4d611ccb58ca5dd91b8eefaf1fafdc6ede1edf7ab94b1a9d2326c03dc001df9a5aebb04c1f9ce91f7914ef876b461c5947a15696d3d25e8765

`hummingbird`:
aa63bedf0785970aa29b66f4a979de6fc2fdb3c8b36fe3dc26cd121f2113c5a4ebc3c1c2144b1f78127877b9e0db358df6288b0c5f87cbb62d515073d119170e


## macOS notarized

`hummingbird-macos-notarized-1.1.1.zip`:
72fcd81377cca449e096e8614c653148e5b76cca4f6e24b9c8b54fed8c9b3549965ddca16d2a0dcebe0b5e23744129db8b3b87d669e63e476c9b92017bbf4fb6

`hummingbird`:
3911d49d4eb4577f30ddb39d1f140c695a6f307cb972ac1d1b4cbb13f56c83e83f1c17d9eb7e54b64b43342009424fcd9d522e4b13011a74ee3bf796ebe8a03e
