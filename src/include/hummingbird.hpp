/*
 * hummingbird.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2020 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef HUMMINGBIRD_HPP
#define HUMMINGBIRD_HPP

#define HUMMINGBIRD_NAME            "Hummingbird - AirVPN OpenVPN 3 Client"
#define HUMMINGBIRD_SHORT_NAME      "Hummingbird"
#define HUMMINGBIRD_VERSION         "1.1.1"
#define HUMMINGBIRD_RELEASE_DATE    "7 January 2020"
#define HUMMINGBIRD_FULL_NAME       HUMMINGBIRD_NAME " " HUMMINGBIRD_VERSION
#define RESOURCE_DIRECTORY          "/etc/airvpn"
#define HUMMINGBIRD_LOCK_FILE       RESOURCE_DIRECTORY "/hummingbird.lock"
#define RESOLVDOTCONF_BACKUP        RESOURCE_DIRECTORY "/resolv.conf.airvpnbackup"
#define SYSTEM_DNS_BACKUP_FILE      RESOURCE_DIRECTORY "/systemdns.airvpnbackup"

#define TCP_QUEUE_LIMIT_DEFAULT     8192

#endif
