/*
 * vpnclient.hpp
 *
 * This file is part of AirVPN's Linux/macOS OpenVPN Client software.
 * Copyright (C) 2019-2020 AirVPN (support@airvpn.org) / https://airvpn.org
 *
 * Developed by ProMIND
 *
 * This is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Hummingbird. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef VPNCLIENT_HPP
#define VPNCLIENT_HPP

#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <thread>
#include <memory>
#include <mutex>
#include <vector>
#include <map>
#include <dirent.h>
#include <errno.h>
#include <resolv.h>
#include <arpa/inet.h>
#include <openvpn/common/platform.hpp>

#define USE_TUN_BUILDER

#if defined(OPENVPN_PLATFORM_LINUX)

    #include "dnsmanager.hpp"
    #include "loadmod.h"

#endif

#ifdef OPENVPN_PLATFORM_MAC

    #include <CoreFoundation/CFBundle.h>
    #include <ApplicationServices/ApplicationServices.h>
    #include <SystemConfiguration/SystemConfiguration.h>

#endif

// don't export core symbols
#define OPENVPN_CORE_API_VISIBILITY_HIDDEN

#define OPENVPN_DEBUG_VERBOSE_ERRORS
// #define OPENVPN_DEBUG_CLIPROTO

#include "execproc.h"
#include "localnetwork.hpp"
#include "netfilter.hpp"

// If enabled, don't direct ovpn3 core logging to
// ClientAPI::OpenVPNClient::log() virtual method.
// Instead, logging will go to LogBaseSimple::log().
// In this case, make sure to define:
//   LogBaseSimple log;
// at the top of your main() function to receive
// log messages from all threads.
// Also, note that the OPENVPN_LOG_GLOBAL setting
// MUST be consistent across all compilation units.

#ifdef OPENVPN_USE_LOG_BASE_SIMPLE

    #define OPENVPN_LOG_GLOBAL // use global rather than thread-local  object pointer

    #include <openvpn/log/logbasesimple.hpp>

#endif

#include <client/ovpncli.cpp>

#if defined(OPENVPN_PLATFORM_LINUX)

    // use SITNL by default
    #ifndef OPENVPN_USE_IPROUTE2

        #define OPENVPN_USE_SITNL

    #endif

    #include <openvpn/tun/linux/client/tuncli.hpp>

    // we use a static polymorphism and define a
    // platform-specific TunSetup class, responsible
    // for setting up tun device

    #define TUN_CLASS_SETUP TunLinuxSetup::Setup<TUN_LINUX>

#elif defined(OPENVPN_PLATFORM_MAC)

    #include <openvpn/tun/mac/client/tuncli.hpp>

    #define TUN_CLASS_SETUP TunMac::Setup

#endif

// should be included before other openvpn includes,
// with the exception of openvpn/log includes

#include <openvpn/common/exception.hpp>
#include <openvpn/common/string.hpp>
#include <openvpn/common/signal.hpp>
#include <openvpn/common/file.hpp>
#include <openvpn/common/getopt.hpp>
#include <openvpn/common/getpw.hpp>
#include <openvpn/common/cleanup.hpp>
#include <openvpn/time/timestr.hpp>
#include <openvpn/ssl/peerinfo.hpp>
#include <openvpn/ssl/sslchoose.hpp>

#ifdef OPENVPN_REMOTE_OVERRIDE

    #include <openvpn/common/process.hpp>

#endif

#if defined(USE_MBEDTLS)

    #include <openvpn/mbedtls/util/pkcs1.hpp>

#endif

enum IPClass
{
    v4,
    v6,
    Unknown
};

using namespace openvpn;

class VpnClientBase : public ClientAPI::OpenVPNClient, public LocalNetwork
{
    protected:

    NetFilter *netFilter = NULL;

#if defined(OPENVPN_PLATFORM_LINUX)
    DNSManager *dnsManager = NULL;
#endif

    std::vector<IPEntry> dnsTable;
    std::vector<IPEntry> systemDnsTable;
    std::vector<IPEntry> remoteServerIpList;
    TUN_CLASS_SETUP::Config tunnelConfig;
    ClientAPI::Config config;
    ClientAPI::EvalConfig evalConfig;
    bool dnsHasBeenPushed = false;
    NetFilter::Mode networkLockMode = NetFilter::Mode::UNKNOWN;
    bool dnsPushIgnored = false;

    std::string resourceDirectory;
    std::string dnsBackupFile;
    std::string resolvDotConfFile;

    public:

    VpnClientBase(NetFilter::Mode netFilterMode, std::string resDir, std::string dnsBkpFile, std::string resolvConfFile);
    ~VpnClientBase();

    bool tun_builder_new() override;
    int tun_builder_establish() override;
    bool tun_builder_add_address(const std::string& address, int prefix_length, const std::string& gateway, bool ipv6, bool net30) override;
    bool tun_builder_add_route(const std::string& address, int prefix_length, int metric, bool ipv6) override;
    bool tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) override;
    bool tun_builder_set_remote_address(const std::string& address, bool ipv6) override;
    bool tun_builder_set_session_name(const std::string& name) override;
    bool tun_builder_add_dns_server(const std::string& address, bool ipv6) override;
    void tun_builder_teardown(bool disconnect) override;
    bool socket_protect(int socket, std::string remote, bool ipv6) override;
    bool ignore_dns_push() override;

    void setConfig(ClientAPI::Config c);
    ClientAPI::Config getConfig();
    void setEvalConfig(ClientAPI::EvalConfig e);
    ClientAPI::EvalConfig getEvalConfig();
    void setNetworkLockMode(NetFilter::Mode mode);
    NetFilter::Mode getNetworkLockMode();
    bool isNetworkLockEnabled();
    void ignoreDnsPush(bool ignore);
    bool isDnsPushIgnored();

    private:

    TUN_CLASS_SETUP::Ptr tunnelSetup = new TUN_CLASS_SETUP();
    TunBuilderCapture tunnelBuilderCapture;
};

class VpnClient : public VpnClientBase
{
    public:

    enum ClockTickAction
    {
        CT_UNDEF,
        CT_STOP,
        CT_RECONNECT,
        CT_PAUSE,
        CT_RESUME,
        CT_STATS,
    };

    enum Status
    {
        DISCONNECTED,
        CONNECTED,
        CONNECTING,
        RECONNECTING,
        PAUSED,
        RESUMING,
        CLIENT_RESTART,
        CONNECTION_TIMEOUT,
        INACTIVE_TIMEOUT
    };

    enum RestoreNetworkMode
    {
        FULL,
        DNS_ONLY,
        FIREWALL_ONLY
    };

    VpnClient(NetFilter::Mode netFilterMode, std::string resDir, std::string dnsBkpFile, std::string resolvConfFile);
    ~VpnClient();

    Status getStatus();
    bool is_dynamic_challenge() const;
    std::string dynamic_challenge_cookie();

    std::string epki_ca;
    std::string epki_cert;

#if defined(USE_MBEDTLS)

    MbedTLSPKI::PKContext epki_ctx; // external PKI context

#endif

    void set_clock_tick_action(const ClockTickAction action);
    void print_stats();
    std::map<std::string, std::string> get_connection_stats();

    bool restoreNetworkSettings(RestoreNetworkMode mode = RestoreNetworkMode::FULL, bool stdOutput = false);

#ifdef OPENVPN_REMOTE_OVERRIDE

    void set_remote_override_cmd(const std::string& cmd);

#endif

    void setConnectionInformation(std::string s);

    private:

    virtual void event(const ClientAPI::Event& ev) override;
    virtual void log(const ClientAPI::LogInfo& log) override;
    virtual void clock_tick() override;
    virtual void external_pki_cert_request(ClientAPI::ExternalPKICertRequest& certreq) override;
    virtual void external_pki_sign_request(ClientAPI::ExternalPKISignRequest& signreq) override;

    static int rng_callback(void *arg, unsigned char *data, size_t len);

    virtual bool pause_on_connection_timeout() override;

#ifdef OPENVPN_REMOTE_OVERRIDE

    virtual bool remote_override_enabled() override;
    virtual void remote_override(ClientAPI::RemoteOverride& ro);

#endif

#if defined(OPENVPN_PLATFORM_LINUX)

    bool loadLinuxModule(std::string module_name, std::string module_params, bool stdOutput = false);
    bool loadLinuxModule(const char *module_name, const char *module_params, bool stdOutput = false);

#endif

    bool addServer(IPClass ipclass, std::string serverIP);

    void onResolveEvent(const ClientAPI::Event ev);
    void onDynamicChallengeEvent(const ClientAPI::Event ev);
    void onConnectedEvent(const ClientAPI::Event ev);
    void onReconnectingEvent(const ClientAPI::Event ev);
    void onDisconnectedEvent(const ClientAPI::Event ev);

    std::mutex log_mutex;
    std::string dc_cookie;
    std::string connectionInformation;

    RandomAPI::Ptr rng;      // random data source for epki
    volatile ClockTickAction clock_tick_action = CT_UNDEF;
    Status status;

#ifdef OPENVPN_REMOTE_OVERRIDE

    std::string remote_override_cmd;

#endif

};

using namespace openvpn;

// VpnClientBase class

VpnClientBase::VpnClientBase(NetFilter::Mode netFilterMode, std::string resDir, std::string dnsBkpFile, std::string resolvConfFile)
{
    resourceDirectory = resDir;
    dnsBackupFile = dnsBkpFile;
    resolvDotConfFile = resolvConfFile;

    netFilter = new NetFilter(resourceDirectory, netFilterMode);

#if defined(OPENVPN_PLATFORM_LINUX)

    dnsManager = new DNSManager(resolvDotConfFile);

#endif

    dnsHasBeenPushed = false;
    networkLockMode = NetFilter::Mode::AUTO;
    dnsPushIgnored = false;
}

VpnClientBase::~VpnClientBase()
{
    if(netFilter != NULL)
        delete netFilter;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsManager != NULL)
        delete dnsManager;

#endif
}

bool VpnClientBase::tun_builder_new()
{
    tunnelBuilderCapture.tun_builder_set_mtu(1500);

    return true;
}

int VpnClientBase::tun_builder_establish()
{
    if(!tunnelSetup)
        tunnelSetup.reset(new TUN_CLASS_SETUP());

    tunnelConfig.layer = Layer(Layer::Type::OSI_LAYER_3);

    // no need to add bypass routes on establish since we do it on socket_protect

    tunnelConfig.add_bypass_routes_on_establish = false;

    return tunnelSetup->establish(tunnelBuilderCapture, &tunnelConfig, nullptr, std::cout);
}

bool VpnClientBase::tun_builder_add_address(const std::string& address, int prefix_length, const std::string& gateway, bool ipv6, bool net30)
{
    return tunnelBuilderCapture.tun_builder_add_address(address, prefix_length, gateway, ipv6, net30);
}

bool VpnClientBase::tun_builder_add_route(const std::string& address, int prefix_length, int metric, bool ipv6)
{
    return tunnelBuilderCapture.tun_builder_add_route(address, prefix_length, metric, ipv6);
}

bool VpnClientBase::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags)
{
    return tunnelBuilderCapture.tun_builder_reroute_gw(ipv4, ipv6, flags);
}

bool VpnClientBase::tun_builder_set_remote_address(const std::string& address, bool ipv6)
{
    return tunnelBuilderCapture.tun_builder_set_remote_address(address, ipv6);
}

bool VpnClientBase::tun_builder_set_session_name(const std::string& name)
{
    return tunnelBuilderCapture.tun_builder_set_session_name(name);
}

bool VpnClientBase::tun_builder_add_dns_server(const std::string& address, bool ipv6)
{
    std::ostringstream os;
    IPEntry dnsEntry;
    NetFilter::IP filterIP;
    std::string dnsFilterIP;

    if(dnsPushIgnored == false)
    {
        os.str("");

        os << "VPN Server has pushed ";

        if(ipv6)
            os << "IPv6";
        else
            os << "IPv4";

        os << " DNS server " << address;

        OPENVPN_LOG(os.str());

        dnsEntry.address = address;
        dnsEntry.ipv6 = ipv6;

        dnsTable.push_back(dnsEntry);

        dnsHasBeenPushed = true;

        dnsFilterIP = address;

        if(ipv6)
        {
            filterIP = NetFilter::IP::v6;

            dnsFilterIP += "/128";
        }
        else
        {
            filterIP = NetFilter::IP::v4;

            dnsFilterIP += "/32";
        }

        netFilter->commitAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dnsFilterIP, 0);
    }

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;

    if(dnsPushIgnored == false)
    {
        os.str("");

        retval = dnsManager->addAddressToResolvDotConf(address, ipv6);

        if(retval == DNSManager::Error::OK)
        {
            os << "Setting pushed ";

            if(ipv6)
                os << "IPv6";
            else
                os << "IPv4";

            os << " DNS server " << address << " in resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "ERROR: Cannot open resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RENAME_ERROR)
        {
            os << "ERROR: Cannot create a backup copy of resolv.conf";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_WRITE_ERROR)
        {
            os << "ERROR: Cannot write in resolv.conf";
        }
        else
        {
            os << "ERROR: resolv.conf generic error";
        }

        OPENVPN_LOG(os.str());

        if(dnsManager->systemHasResolved())
        {
            for(std::string interface : localInterface)
            {
                os.str("");

                retval = dnsManager->addAddressToResolved(interface, address.c_str(), ipv6);

                if(retval == DNSManager::Error::OK)
                {
                    os << "Setting pushed ";

                    if(ipv6)
                        os << "IPv6";
                    else
                        os << "IPv4";

                    os << " DNS server " << address << " for interface " << interface << " via systemd-resolved";
                }
                else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                {
                    os << "ERROR systemd-resolved is not available on this system";
                }
                else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                {
                    os << "ERROR systemd-resolved: Failed to add DNS server " << address << " for interface " << interface;
                }
                else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                {
                    os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                }
                else
                {
                    os << "ERROR systemd-resolved: Unknown error while adding DNS server " << address << " for interface " << interface;
                }

                OPENVPN_LOG(os.str());
            }
        }
    }
    else
    {
        os.str("");

        os << "WARNING: ignoring server DNS push request for address " << address;

        OPENVPN_LOG(os.str());
    }

#endif

    return tunnelBuilderCapture.tun_builder_add_dns_server(address, ipv6);
}

void VpnClientBase::tun_builder_teardown(bool disconnect)
{
    std::ostringstream os;

    auto os_print = Cleanup([&os](){ OPENVPN_LOG_STRING(os.str()); });

    tunnelSetup->destroy(os);
}

bool VpnClientBase::socket_protect(int socket, std::string remote, bool ipv6)
{
    (void)socket;
    std::ostringstream os;

    auto os_print = Cleanup([&os](){ OPENVPN_LOG_STRING(os.str()); });

    return tunnelSetup->add_bypass_route(remote, ipv6, os);
}

bool VpnClientBase::ignore_dns_push()
{
    return dnsPushIgnored;
}

void VpnClientBase::setConfig(ClientAPI::Config c)
{
    config = c;
}

ClientAPI::Config VpnClientBase::getConfig()
{
    return config;
}

void VpnClientBase::setEvalConfig(ClientAPI::EvalConfig e)
{
    evalConfig = e;
}

ClientAPI::EvalConfig VpnClientBase::getEvalConfig()
{
    return evalConfig;
}

void VpnClientBase::setNetworkLockMode(NetFilter::Mode mode)
{
    networkLockMode = mode;

    netFilter->setMode(mode);
}

NetFilter::Mode VpnClientBase::getNetworkLockMode()
{
    return networkLockMode;
}

bool VpnClientBase::isNetworkLockEnabled()
{
    bool networkLockEnabled = true;

    if(netFilter->getMode() == NetFilter::Mode::OFF || netFilter->getMode() == NetFilter::Mode::UNKNOWN)
        networkLockEnabled = false;

    return networkLockEnabled;
}

void VpnClientBase::ignoreDnsPush(bool ignore)
{
    dnsPushIgnored = ignore;
}

bool VpnClientBase::isDnsPushIgnored()
{
    return dnsPushIgnored;
}

// VpnClient class

VpnClient::VpnClient(NetFilter::Mode netFilterMode, std::string resDir, std::string dnsBkpFile, std::string resolvConfFile) : VpnClientBase(netFilterMode, resDir, dnsBkpFile, resolvConfFile)
{
    status = Status::DISCONNECTED;
    
    connectionInformation = "";
}

VpnClient::~VpnClient()
{
}

VpnClient::Status VpnClient::getStatus()
{
    return status;
}

void VpnClient::setConnectionInformation(std::string s)
{
    connectionInformation = s;
}

bool VpnClient::is_dynamic_challenge() const
{
    return !dc_cookie.empty();
}

std::string VpnClient::dynamic_challenge_cookie()
{
    return dc_cookie;
}

void VpnClient::set_clock_tick_action(const ClockTickAction action)
{
    clock_tick_action = action;
}

void VpnClient::print_stats()
{
    const int n = stats_n();
    std::vector<long long> stats = stats_bundle();

    std::cout << "STATS:" << std::endl;

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];

        if(value)
            std::cout << "  " << stats_name(i) << " : " << value << std::endl;
    }
}

std::map<std::string, std::string> VpnClient::get_connection_stats()
{
    int n;
    std::vector<long long> stats;
    std::map<std::string, std::string> conn_stats;
    openvpn::ClientAPI::ConnectionInfo connectionInfo;

    connectionInfo = connection_info();

    conn_stats.insert(std::make_pair("USER", connectionInfo.user));
    conn_stats.insert(std::make_pair("SERVER_HOST", connectionInfo.serverHost));
    conn_stats.insert(std::make_pair("SERVER_PORT", connectionInfo.serverPort));
    conn_stats.insert(std::make_pair("SERVER_PROTO", connectionInfo.serverProto));
    conn_stats.insert(std::make_pair("SERVER_IP", connectionInfo.serverIp));
    conn_stats.insert(std::make_pair("VPN_IP4", connectionInfo.vpnIp4));
    conn_stats.insert(std::make_pair("VPN_IP6", connectionInfo.vpnIp6));
    conn_stats.insert(std::make_pair("GATEWAY_IPV4", connectionInfo.gw4));
    conn_stats.insert(std::make_pair("GATEWAY_IPV6", connectionInfo.gw6));
    conn_stats.insert(std::make_pair("CLIENT_IP", connectionInfo.clientIp));
    conn_stats.insert(std::make_pair("TUN_NAME", connectionInfo.tunName));

    n = stats_n();
    stats = stats_bundle();

    for(int i = 0; i < n; ++i)
    {
        const long long value = stats[i];

        if(value)
            conn_stats.insert(std::make_pair(stats_name(i), std::to_string(value)));
    }
    
    return conn_stats;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

    void VpnClient::set_remote_override_cmd(const std::string& cmd)
    {
        remote_override_cmd = cmd;
    }

#endif

void VpnClient::event(const ClientAPI::Event& ev)
{
    std::ostringstream os;

    os.str("");

    os << "EVENT: " << ev.name;

    if(!ev.info.empty())
        os << ' ' << ev.info;

    if(ev.fatal)
        os << " [FATAL-ERR]";
    else if(ev.error)
        os << " [ERR]";

    OPENVPN_LOG(os.str());

    if(ev.name == "RESOLVE")
    {
        onResolveEvent(ev);
    }
    else if(ev.name == "DYNAMIC_CHALLENGE")
    {
        onDynamicChallengeEvent(ev);
    }
    else if(ev.name == "CONNECTED")
    {
        onConnectedEvent(ev);
    }
    else if(ev.name == "RECONNECTING")
    {
        onReconnectingEvent(ev);
    }
    else if(ev.name == "DISCONNECTED")
    {
        onDisconnectedEvent(ev);
    }
    else if(ev.name == "CONNECTING")
    {
        status = Status::CONNECTING;
    }
    else if(ev.name == "PAUSED")
    {
        status = Status::PAUSED;
    }
    else if(ev.name == "RESUMING")
    {
        status = Status::RESUMING;
    }
    else if(ev.name == "CLIENT_RESTART")
    {
        status = Status::CLIENT_RESTART;
    }
    else if(ev.name == "CONNECTION_TIMEOUT")
    {
        status = Status::CONNECTION_TIMEOUT;
    }
    else if(ev.name == "INACTIVE_TIMEOUT")
    {
        status = Status::INACTIVE_TIMEOUT;
    }
}

bool VpnClient::addServer(IPClass ipclass, std::string serverIP)
{
    std::string filterServerIP;
    IPEntry ipEntry;
    NetFilter::IP filterIPLevel;
    bool res = false;

    if(ipclass == IPClass::Unknown || serverIP == "" || serverIP == "0.0.0.0" || serverIP == "::/0")
        return false;

    ipEntry.address = serverIP;

    if(ipclass == IPClass::v4)
    {
        filterIPLevel = NetFilter::IP::v4;

        filterServerIP = serverIP + "/32";

        ipEntry.ipv6 = false;
    }
    else
    {
        filterIPLevel = NetFilter::IP::v6;

        filterServerIP = serverIP + "/128";

        ipEntry.ipv6 = true;
    }

    if(std::find(remoteServerIpList.begin(), remoteServerIpList.end(), ipEntry) == remoteServerIpList.end())
    {
        netFilter->addAllowRule(filterIPLevel, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, filterServerIP, 0);

        remoteServerIpList.push_back(ipEntry);

        res = true;
    }

    return res;
}

void VpnClient::onResolveEvent(const ClientAPI::Event ev)
{
    NetFilter::Mode filterMode;
    std::string serverIP, filterServerIP, protocol;
    std::ofstream systemDNSDumpFile;
    std::ostringstream os;
    struct addrinfo ipinfo, *ipres = NULL, *current_ai;
    int aires;
    char resip[64];
    IPClass ipclass;
    NetFilter::IP filterIP;
    IPEntry ipEntry;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(status != Status::RECONNECTING)
    {
        if(dnsPushIgnored == false)
        {
            if(dnsManager->systemHasNetworkManager())
            {
                os.str("");

                os << "WARNING: NetworkManager is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }

            if(dnsManager->systemHasResolved())
            {
                os.str("");

                os << "WARNING: systemd-resolved is running on this system and may interfere with DNS management and cause DNS leaks";

                OPENVPN_LOG(os.str());
            }
        }
    }

#endif

    if(status != Status::RECONNECTING)
    {
        // save system DNS

        res_ninit(&_res);

        dnsTable.clear();
        systemDnsTable.clear();

        systemDNSDumpFile.open(dnsBackupFile);

        for(int i=0; i < MAXNS; i++)
        {
            if(_res.nsaddr_list[i].sin_addr.s_addr > 0)
            {
                ipEntry.address = inet_ntoa(_res.nsaddr_list[i].sin_addr);

                if(_res.nsaddr_list[i].sin_family == AF_INET)
                    ipEntry.ipv6 = false;
                else
                    ipEntry.ipv6 = true;

                systemDnsTable.push_back(ipEntry);

                systemDNSDumpFile << ipEntry.address << std::endl;
            }
        }

        systemDNSDumpFile.close();
    }

    if(isNetworkLockEnabled())
    {
        if(status != Status::RECONNECTING)
        {
            os.str("");

            filterMode = netFilter->getMode();

            if(filterMode != NetFilter::Mode::AUTO && filterMode != NetFilter::Mode::UNKNOWN)
                os << "Network filter and lock is using " << netFilter->getModeDescription();
            else
                os << "Network filter and lock disabled. No supported backend found.";

            OPENVPN_LOG(os.str());

    #if defined(OPENVPN_PLATFORM_LINUX)

            if(filterMode == NetFilter::Mode::IPTABLES)
            {
                loadLinuxModule("iptable_filter", "");
                loadLinuxModule("iptable_nat", "");
                loadLinuxModule("iptable_mangle", "");
                loadLinuxModule("iptable_security", "");
                loadLinuxModule("iptable_raw", "");

                loadLinuxModule("ip6table_filter", "");
                loadLinuxModule("ip6table_nat", "");
                loadLinuxModule("ip6table_mangle", "");
                loadLinuxModule("ip6table_security", "");
                loadLinuxModule("ip6table_raw", "");
            }
            else if(filterMode == NetFilter::Mode::NFTABLES)
            {
                loadLinuxModule("nf_tables", "");
            }

            if(netFilter->isFirewalldRunning())
            {
                os.str("");

                os << "WARNING: firewalld is running on this system and may interfere with network filter and lock";

                OPENVPN_LOG(os.str());
            }

            if(netFilter->isUfwRunning())
            {
                os.str("");

                os << "WARNING: ufw is running on this system and may interfere with network filter and lock";

                OPENVPN_LOG(os.str());
            }

    #endif

            os.str("");

            if(netFilter->init())
                os << "Network filter successfully initialized";
            else
                os << "ERROR: Cannot initialize network filter";

            OPENVPN_LOG(os.str());

            for(IPEntry ip : localIPaddress)
            {
                os.str("");

                os << "Local IPv";

                if(ip.ipv6 == true)
                    os << "6";
                else
                    os << "4";

                os << " address " << ip.address;

                OPENVPN_LOG(os.str());
            }

            for(std::string interface : localInterface)
            {
                os.str("");

                os << "Local interface " << interface;

                OPENVPN_LOG(os.str());
            }

            netFilter->setup(loopbackInterface);
        }

        os.str("");

        os << "Setting up network filter and lock";

        OPENVPN_LOG(os.str());

        // Allow system DNS to pass through network filter. It may be later denied by DNS push

        for(IPEntry dns : systemDnsTable)
        {
            if(dns.ipv6 == true)
                filterIP = NetFilter::IP::v6;
            else
                filterIP = NetFilter::IP::v4;

            netFilter->addAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

            os.str("");

            os << "Allowing system DNS " << dns.address << " to pass through the network filter";

            OPENVPN_LOG(os.str());
        }

        if(status == Status::RECONNECTING)
        {
            if(netFilter->commitRules() == false)
            {
                os.str("");

                os << "ERROR: Cannot allow system DNS to pass through network filter";

                OPENVPN_LOG(os.str());
            }
        }

        // Adding profile's remote entries to network filter

        if(evalConfig.remoteList.size() > 1)
        {
            os.str("");

            os << "OpenVPN profile has multiple remote directives. Temporarily adding remote servers to network filter.";

            OPENVPN_LOG(os.str());
        }

        remoteServerIpList.clear();

        for(ClientAPI::RemoteEntry remoteEntry : evalConfig.remoteList)
        {
            memset(&ipinfo, 0, sizeof(ipinfo));

            ipinfo.ai_family = PF_UNSPEC;
            ipinfo.ai_flags = AI_NUMERICHOST;
            ipclass = IPClass::Unknown;

            aires = getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres);

            if(aires || ipres == NULL)
            {
                ipclass = IPClass::Unknown;
            }
            else
            {
                serverIP = remoteEntry.server;

                switch(ipres->ai_family)
                {
                    case AF_INET:
                    {
                        ipclass = IPClass::v4;
                    }
                    break;

                    case AF_INET6:
                    {
                        ipclass = IPClass::v6;
                    }
                    break;

                    default:
                    {
                        ipclass = IPClass::Unknown;
                    }
                }
            }

            if(ipres != NULL)
            {
                freeaddrinfo(ipres);

                ipres = NULL;
            }

            if(ipclass == IPClass::Unknown)
            {
                memset(&ipinfo, 0, sizeof(ipinfo));

                ipinfo.ai_family = PF_UNSPEC;

                os.str("");

                if(getaddrinfo(remoteEntry.server.c_str(), NULL, &ipinfo, &ipres) == 0)
                {
                    for(current_ai = ipres; current_ai != NULL; current_ai = current_ai->ai_next)
                    {
                        getnameinfo(current_ai->ai_addr, current_ai->ai_addrlen, resip, sizeof(resip), NULL, 0, NI_NUMERICHOST);

                        serverIP = resip;

                        switch(current_ai->ai_family)
                        {
                            case AF_INET:
                            {
                                protocol = "4";
                                ipclass = IPClass::v4;
                            }
                            break;

                            case AF_INET6:
                            {
                                protocol = "6";
                                ipclass = IPClass::v6;
                            }
                            break;

                            default:
                            {
                                protocol = "??";
                                ipclass = IPClass::Unknown;
                            }
                        }

                        if(addServer(ipclass, serverIP) == true)
                        {
                            os.str("");

                            os << "Resolved server " << remoteEntry.server << " into IPv" << protocol<< " " << serverIP;

                            OPENVPN_LOG(os.str());;

                            os.str("");

                            os << "Adding IPv" << protocol << " server " << serverIP << " to network filter";

                            OPENVPN_LOG(os.str());
                        }
                    }
                }
                else
                {
                    ipclass = IPClass::Unknown;
                    serverIP = "";

                    os.str("");

                    os << "WARNING: Cannot resolve " << remoteEntry.server;

                    OPENVPN_LOG(os.str());
                }

                if(ipres != NULL)
                {
                    freeaddrinfo(ipres);

                    ipres = NULL;
                }
            }
            else
            {
                if(addServer(ipclass, serverIP) == true)
                {
                    os.str("");

                    os << "Adding IPv";

                    if(ipclass == IPClass::v4)
                        os << "4";
                    else
                        os << "6";

                    os << " server " << serverIP << " to network filter";

                    OPENVPN_LOG(os.str());
                }
            }
        }

        os.str("");

        if(netFilter->commitRules() == true)
            os << "Network filter and lock successfully activated";
        else
            os << "ERROR: Cannot activate network filter and lock";

        OPENVPN_LOG(os.str());
    }
    else
    {
        os.str("");

        os << "WARNING: Network filter and lock is disabled";

        OPENVPN_LOG(os.str());
    }
}

void VpnClient::onDynamicChallengeEvent(const ClientAPI::Event ev)
{
    std::ostringstream os;

    dc_cookie = ev.info;

    ClientAPI::DynamicChallenge dc;

    if(ClientAPI::OpenVPNClient::parse_dynamic_challenge(ev.info, dc))
    {
        os << "DYNAMIC CHALLENGE" << std::endl;
        os << "challenge: " << dc.challenge << std::endl;
        os << "echo: " << dc.echo << std::endl;
        os << "responseRequired: " << dc.responseRequired << std::endl;
        os << "stateID: " << dc.stateID << std::endl;

        OPENVPN_LOG(os.str());
    }
}

void VpnClient::onConnectedEvent(const ClientAPI::Event ev)
{
    std::ostringstream os;
    NetFilter::IP filterIP;

    if(connectionInformation != "")
        OPENVPN_LOG(connectionInformation);

    openvpn::ClientAPI::ConnectionInfo connectionInfo = connection_info();

    netFilter->addIgnoredInterface(tunnelConfig.iface_name);

#if defined(OPENVPN_PLATFORM_LINUX)

    DNSManager::Error retval;

    if(dnsManager->systemHasResolved())
    {
        for(std::string interface : localInterface)
        {
            if(strncmp(connectionInfo.tunName.c_str(), interface.c_str(), connectionInfo.tunName.length()) == 0)
            {
                for(IPEntry dns : dnsTable)
                {
                    os.str("");

                    retval = dnsManager->addAddressToResolved(interface, dns.address.c_str(), dns.ipv6);

                    if(retval == DNSManager::Error::OK)
                    {
                        os << "Setting pushed ";

                        if(dns.ipv6)
                            os << "IPv6";
                        else
                            os << "IPv4";

                        os << " DNS server " << dns.address << " for interface " << interface << " via systemd-resolved";
                    }
                    else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
                    {
                        os << "ERROR systemd-resolved is not available on this system";
                    }
                    else if(retval == DNSManager::Error::RESOLVED_ADD_DNS_ERROR)
                    {
                        os << "ERROR systemd-resolved: Failed to add DNS server " << dns.address << " for interface " << interface;
                    }
                    else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
                    {
                        os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";
                    }
                    else
                    {
                        os << "ERROR systemd-resolved: Unknown error while adding DNS server " << dns.address << " for interface " << interface;
                    }

                    OPENVPN_LOG(os.str());
                }
            }
        }
    }

#endif

    if(dnsHasBeenPushed == true && isNetworkLockEnabled() && dnsPushIgnored == false)
    {
        os.str("");

        os << "Server has pushed its own DNS. Removing system DNS from network filter.";

        OPENVPN_LOG(os.str());

        for(IPEntry dns : systemDnsTable)
        {
            if(dns.ipv6 == true)
                filterIP = NetFilter::IP::v6;
            else
                filterIP = NetFilter::IP::v4;

            netFilter->commitRemoveAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, dns.address, 0);

            os.str("");

            os << "System DNS " << dns.address << " is now rejected by the network filter";

            OPENVPN_LOG(os.str());
        }
    }

    if(isNetworkLockEnabled() && remoteServerIpList.size() > 1)
    {
        os.str("");

        os << "OpenVPN profile has multiple remote directives. Removing unused servers from network filter.";

        OPENVPN_LOG(os.str());

        for(IPEntry server : remoteServerIpList)
        {
            if(server.address != connectionInfo.serverIp)
            {
                if(server.ipv6 == true)
                    filterIP = NetFilter::IP::v6;
                else
                    filterIP = NetFilter::IP::v4;

                netFilter->commitRemoveAllowRule(filterIP, NetFilter::Direction::OUTPUT, "", NetFilter::Protocol::ANY, "", 0, server.address, 0);

                os.str("");

                os << "Server IPv";

                if(server.ipv6 == false)
                    os << "4";
                else
                    os << "6";

                os << " " << server.address << " has been removed from the network filter";

                OPENVPN_LOG(os.str());
            }
        }
    }

    status = Status::CONNECTED;
}

void VpnClient::onReconnectingEvent(const ClientAPI::Event ev)
{
    if(config.tunPersist == false)
        restoreNetworkSettings(RestoreNetworkMode::DNS_ONLY);

    status = Status::RECONNECTING;
}

void VpnClient::onDisconnectedEvent(const ClientAPI::Event ev)
{
    restoreNetworkSettings();

    status = Status::DISCONNECTED;
}

bool VpnClient::restoreNetworkSettings(RestoreNetworkMode mode, bool stdOutput)
{
    std::ostringstream os;
    bool success = true;

#if defined(OPENVPN_PLATFORM_LINUX)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        DNSManager::Error retval;

        // restore resolv.conf

        retval = dnsManager->restoreResolvDotConf();

        os.str("");

        if(retval == DNSManager::Error::OK)
        {
            os << "Successfully restored DNS settings";
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_OPEN_ERROR)
        {
            os << "ERROR: Cannot restore DNS settings. resolv.conf not found.";

            success = false;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_NOT_FOUND)
        {
            os << "ERROR: Backup copy of resolv.conf not found.";

            success = false;
        }
        else if(retval == DNSManager::Error::RESOLV_DOT_CONF_RESTORE_ERROR)
        {
            os << "ERROR: Cannot restore DNS settings.";

            success = false;
        }
        else
        {
            os << "ERROR: Cannot restore DNS settings. Unknown error.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();

        if(dnsManager->systemHasResolved())
        {
            os.str("");

            retval = dnsManager->revertAllResolved();

            if(retval == DNSManager::Error::OK)
            {
                os << "Reverting systemd-resolved DNS settings";
            }
            else if(retval == DNSManager::Error::RESOLVED_IS_NOT_AVAILABLE)
            {
                os << "ERROR systemd-resolved is not available on this system";

                success = false;
            }
            else if(retval == DNSManager::Error::RESOLVED_REVERT_DNS_ERROR)
            {
                os << "ERROR systemd-resolved: Failed to revert DNS servers";

                success = false;
            }
            else if(retval == DNSManager::Error::NO_RESOLVED_COMMAND)
            {
                os << "ERROR systemd-resolved: resolvectl or systemd-resolve command not found";

                success = false;
            }
            else
            {
                os << "ERROR systemd-resolved: Unknown error while reverting DNS";

                success = false;
            }

            os << std::endl;

            if(stdOutput == false)
                OPENVPN_LOG_STRING(os.str());
            else
                std::cout << os.str();
        }
    }

#endif

#if defined(OPENVPN_PLATFORM_MAC)

    if(dnsPushIgnored == false && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::DNS_ONLY))
    {
        std::ifstream systemDNSDumpFile;
        std::string line;
        SCDynamicStoreRef dnsStore;
        CFMutableArrayRef dnsArray;
        CFDictionaryRef dnsDictionary;
        CFArrayRef dnsList;
        CFIndex ndx, listItems;
        bool setValueSuccess = true;

        os.str("");

        if(access(dnsBackupFile.c_str(), F_OK) == 0)
        {
            dnsArray = CFArrayCreateMutable(NULL, 0, NULL);

            systemDNSDumpFile.open(dnsBackupFile);

            while(std::getline(systemDNSDumpFile, line))
                CFArrayAppendValue(dnsArray, CFStringCreateWithFormat(NULL, NULL, CFSTR("%s"), line.c_str()));

            systemDNSDumpFile.close();

            dnsStore = SCDynamicStoreCreate(kCFAllocatorSystemDefault, CFSTR("AirVPNDNSRestore"), NULL, NULL);

            dnsDictionary = CFDictionaryCreate(NULL, (const void **)(CFStringRef []){ CFSTR("ServerAddresses") }, (const void **)&dnsArray, 1, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);

            dnsList = SCDynamicStoreCopyKeyList(dnsStore, CFSTR("Setup:/Network/(Service/.+|Global)/DNS"));

            listItems = CFArrayGetCount(dnsList);

            if(listItems > 0)
            {
                ndx = 0;

                while(ndx < listItems)
                {
                    setValueSuccess &= SCDynamicStoreSetValue(dnsStore, (CFStringRef)CFArrayGetValueAtIndex(dnsList, ndx), dnsDictionary);

                    ndx++;
                }
            }

            if(setValueSuccess == true)
                os << "Successfully restored system DNS.";
            else
            {
                os << "ERROR: Error while restoring DNS settings.";

                success = false;
            }
        }
        else
        {
            os << "ERROR: Cannot restore DNS settings. Backup copy of system DNS not found.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

#endif

    unlink(dnsBackupFile.c_str());

    os.str("");

    if(isNetworkLockEnabled() && (mode == RestoreNetworkMode::FULL || mode == RestoreNetworkMode::FIREWALL_ONLY))
    {
        if(netFilter->restore())
            os << "Network filter successfully restored";
        else
        {
            os << "ERROR: Backup copy of network filter not found.";

            success = false;
        }

        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

    return success;
}

void VpnClient::log(const ClientAPI::LogInfo& log)
{
    std::lock_guard<std::mutex> lock(log_mutex);

    std::cout << date_time() << ' ' << log.text << std::flush;
}

void VpnClient::clock_tick()
{
    const ClockTickAction action = clock_tick_action;
    clock_tick_action = CT_UNDEF;

    switch(action)
    {
        case CT_STOP:
        {
            std::cout << "signal: CT_STOP" << std::endl;

            stop();
        }
        break;

        case CT_RECONNECT:
        {
            std::cout << "signal: CT_RECONNECT" << std::endl;

            reconnect(0);
        }
        break;

        case CT_PAUSE:
        {
            std::cout << "signal: CT_PAUSE" << std::endl;

            pause("clock-tick pause");
        }
        break;

        case CT_RESUME:
        {
            std::cout << "signal: CT_RESUME" << std::endl;

            resume();
        }
        break;

        case CT_STATS:
        {
            std::cout << "signal: CT_STATS" << std::endl;

            print_stats();
        }
        break;

        default: break;
    }
}

void VpnClient::external_pki_cert_request(ClientAPI::ExternalPKICertRequest& certreq)
{
    if(!epki_cert.empty())
    {
        certreq.cert = epki_cert;
        certreq.supportingChain = epki_ca;
    }
    else
    {
        certreq.error = true;
        certreq.errorText = "external_pki_cert_request not implemented";
    }
}

void VpnClient::external_pki_sign_request(ClientAPI::ExternalPKISignRequest& signreq)
{
#if defined(USE_MBEDTLS)

    if(epki_ctx.defined())
    {
        try
        {
            // decode base64 sign request
            BufferAllocated signdata(256, BufferAllocated::GROW);
            base64->decode(signdata, signreq.data);

            // get MD alg
            const mbedtls_md_type_t md_alg = PKCS1::DigestPrefix::MbedTLSParse().alg_from_prefix(signdata);

            // log info
            OPENVPN_LOG("SIGN[" << PKCS1::DigestPrefix::MbedTLSParse::to_string(md_alg) << ',' << signdata.size() << "]: " << render_hex_generic(signdata));

            // allocate buffer for signature
            BufferAllocated sig(mbedtls_pk_get_len(epki_ctx.get()), BufferAllocated::ARRAY);

            // sign it
            size_t sig_size = 0;

            const int status = mbedtls_pk_sign(epki_ctx.get(), md_alg, signdata.c_data(), signdata.size(), sig.data(), &sig_size, rng_callback, this);

            if(status != 0)
                throw Exception("mbedtls_pk_sign failed, err=" + openvpn::to_string(status));

            if(sig.size() != sig_size)
                throw Exception("unexpected signature size");

            // encode base64 signature

            signreq.sig = base64->encode(sig);

            OPENVPN_LOG("SIGNATURE[" << sig_size << "]: " << signreq.sig);
        }
        catch(const std::exception& e)
        {
            signreq.error = true;
            signreq.errorText = std::string("external_pki_sign_request: ") + e.what();
        }
    }
    else

#endif

    {
        signreq.error = true;
        signreq.errorText = "external_pki_sign_request not implemented";
    }
}

int VpnClient::rng_callback(void *arg, unsigned char *data, size_t len)
{
    VpnClient *self = (VpnClient *)arg;

    if(!self->rng)
    {
        self->rng.reset(new SSLLib::RandomAPI(false));
        self->rng->assert_crypto();
    }

    return self->rng->rand_bytes_noexcept(data, len) ? 0 : -1; // using -1 as a general-purpose mbed TLS error code
}

bool VpnClient::pause_on_connection_timeout()
{
    return false;
}

#ifdef OPENVPN_REMOTE_OVERRIDE

bool VpnClient::remote_override_enabled()
{
    return !remote_override_cmd.empty();
}

void VpnClient::remote_override(ClientAPI::RemoteOverride& ro)
{
    RedirectPipe::InOut pio;
    Argv argv;
    argv.emplace_back(remote_override_cmd);
    OPENVPN_LOG(argv.to_string());

    const int status = system_cmd(remote_override_cmd, argv, nullptr, pio, RedirectPipe::IGNORE_ERR);

    if(!status)
    {
        const std::string out = string::first_line(pio.out);

        OPENVPN_LOG("REMOTE OVERRIDE: " << out);

        auto svec = string::split(out, ',');

        if(svec.size() == 4)
        {
            ro.host = svec[0];
            ro.ip = svec[1];
            ro.port = svec[2];
            ro.proto = svec[3];
        }
        else
            ro.error = "cannot parse remote-override, expecting host,ip,port,proto (at least one or both of host and ip must be defined)";
    }
    else
        ro.error = "status=" + std::to_string(status);
}

#endif

#if defined(OPENVPN_PLATFORM_LINUX)

bool VpnClient::loadLinuxModule(std::string module_name, std::string module_params, bool stdOutput)
{
    return loadLinuxModule(module_name.c_str(), module_params.c_str(), stdOutput);
}

bool VpnClient::loadLinuxModule(const char *module_name, const char *module_params, bool stdOutput)
{
    std::ostringstream os;
    int retval;
    bool result = false;

    retval = load_kernel_module(module_name, module_params);

    os.str("");

    switch(retval)
    {
        case MODULE_LOAD_SUCCESS:
        {
            os << "Successfully loaded kernel module " << module_name;

            result = true;
        }
        break;

        case MODULE_ALREADY_LOADED:
        {
            result = true;
        }
        break;

        case MODULE_NOT_FOUND:
        {
            os << "WARNING: Kernel module " << module_name << " not found. (" << retval << ")";

            result = true;
        }
        break;

        default:
        {
            os << "ERROR: Error while loading kernel module " << module_name << " (" << retval << ")";

            result = true;
        }
        break;
    }

    if(os.str() != "")
    {
        os << std::endl;

        if(stdOutput == false)
            OPENVPN_LOG_STRING(os.str());
        else
            std::cout << os.str();
    }

    return result;
}

#endif

#endif
